﻿using UnityEngine;

namespace toio.tutorial
{
    public class DebugScript : MonoBehaviour
    {
        float intervalTime = 0.05f;
        float elapsedTime = 0;
        Cube[] cubes;
        bool started = false;

        async void Start()
        {
            //Connect 4 Toio
            var peripherals = await new NearScanner(4).Scan();
            cubes = await new CubeConnecter().Connect(peripherals);
            started = true;
        }

        void Update()
        {
            if (!started) { return; }

            elapsedTime += Time.deltaTime;
            if (intervalTime < elapsedTime)
            {
                elapsedTime = 0.0f;
                
                
                //cubes[0].Move(60, 20, 200);
                /*
                foreach (var cube in cubes)
                {
                    cube.Move(60, 20, 200);
                }
                */
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Debug.Log("UpArrow key was pressed.");
                cubes[0].Move(20, 20, 1500);
            }
            else if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                Debug.Log("DownArrow key was pressed");
                cubes[0].Move(-20, -20, 1500);
            }
            else if(Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Debug.Log("LeftArrow key was pressed");
                cubes[0].Move(20, 50, 1800);
            }
            else if(Input.GetKeyDown(KeyCode.RightArrow))
            {
                Debug.Log("RightArrow key was pressed");
                cubes[0].Move(50, -50, 1500);
            }

        }
    }
}