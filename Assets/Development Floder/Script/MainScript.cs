﻿using UnityEngine;
using toio;
using toio.Navigation;
using toio.MathUtils;
using static toio.MathUtils.Utils;
using System.Collections.Generic;

public class MainScript : MonoBehaviour
{
    CubeManager cm;
    List<Vector> islandPosList;
    Vector islandPos1 = new Vector(95, 95);
    Vector islandPos2 = new Vector(450, 60);
    Vector islandPos3 = new Vector(800, 80);
    Vector islandPos4 = new Vector(200, 250);
    Vector islandPos5 = new Vector(600, 275);
    Vector islandPos6 = new Vector(100, 375);
    Vector islandPos7 = new Vector(275, 400);
    Vector islandPos8 = new Vector(550, 430);

    Vector prisonPos = new Vector(300, 440);
    Vector currentIslandPos;
    int islandPosListIndex = 0;

    async void Start()
    {
        //cube manager
        cm = new CubeManager();
        await cm.MultiConnect(4);

        cm.handles.Clear();
        cm.navigators.Clear();

        //declaim 
        //islandPosList.Add(new Vector(95, 95)); 
        //= new List<Vector>((95, 95), (400, 60), (800, 310), (200, 100), (600, 80), (125, 250), (275, 275), (550, 300)); //8 island 
        islandPosList = new List<Vector>();
        islandPosList.Add(islandPos1);
        islandPosList.Add(islandPos2);
        islandPosList.Add(islandPos3);
        islandPosList.Add(islandPos4);
        islandPosList.Add(islandPos5);
        islandPosList.Add(islandPos6);
        islandPosList.Add(islandPos7);
        islandPosList.Add(islandPos8);

        //islandPos = new Vector(95, 95);
        currentIslandPos = islandPosList[islandPosListIndex];

        foreach (var cube in cm.cubes)
        {
            var handle = new HandleMats(cube);
            cm.handles.Add(handle);
            var navi = new CubeNavigator(handle);
            navi.usePred = true;
            navi.mode = Navigator.Mode.BOIDS_AVOID;
            cm.navigators.Add(navi);

            //set map border
            handle.borderRect = new RectInt(95, 95, 720, 720);
            navi.ClearWall();
            navi.AddBorder(30, x1: 0, x2: 910, y1: 0, y2: 455);
        }
    }

    void Update()
    {
        //var tar = Vector.fromRadMag(Time.time / 2, 100) + new Vector(455, 220); //the area which cubes move (circle)
        if (cm.synced)
        {
            //cubes move
            for (int i = 0; i < cm.navigators.Count; i++)
            {
                var navi = cm.navigators[i];
                var mv = navi.Navi2Target(currentIslandPos, maxSpd: 60).Exec(); //new Vector(100,100);
            }
            /*for (int i = 0; i < cm.navigators.Count; i++)
            {
                var navi = cm.navigators[i];
                var mv = navi.Navi2Target(tar, maxSpd: 60).Exec();
            }*/
        }

        //change island
        if (Input.GetKeyDown(KeyCode.A))
        {
            ChangeIsland();
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            currentIslandPos = prisonPos;
        }
    }

    public void ChangeIsland()
    {
        Debug.Log("move to next island:");
        if (islandPosListIndex < 7)
        {
            islandPosListIndex += 1;
        }
        else
        {
            islandPosListIndex = 0;
        }

        currentIslandPos = islandPosList[islandPosListIndex];
        Debug.Log(islandPosListIndex+":");
        Debug.Log(currentIslandPos);
    }

    public class HandleMats : CubeHandle
    {
        public HandleMats(Cube _cube) : base(_cube)
        { }
        //map (2 mat)
        public int matX = 0;
        public int matY = 0;
        protected float lastX = 250;
        protected float lastY = 250;
        public override void Update()
        {
            var frm = Time.frameCount;
            if (frm == updateLastFrm) return;
            updateLastFrm = frm;

            var rawX = cube.x;
            var rawY = cube.y;

            // update matX, matY
            if (lastX > 455 - 50 && rawX < 45 + 50)
                matX += 1;
            else if (rawX > 455 - 50 && lastX < 45 + 50)
                matX -= 1;

            /*if (lastY > 455 - 50 && rawY < 45 + 50)
                matY += 1;
            else if (rawY > 455 - 50 && lastY < 45 + 50)
                matY -= 1;
            */

            if (matX < 0) matX = 0;
            //if (matY < 0) matY = 0;

            lastX = rawX; lastY = rawY;

            // update x, y
            x = rawX + matX * 411;
            y = rawY + matY * 411;
            deg = Deg(cube.angle);
            UpdateProperty();

            Predict();
        }
    }
}
